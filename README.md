# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###
A sample Restful API that performs CRUD operations an an embedded database

### How do I get set up? ###

Download the project and navigate to its directory
build using maven. This will create a .jar file in the target directory
which you can run. From there use a REST client to call to the service

Command to build with maven:
mvn package

A possible rest client to use (chrome app):
https://chrome.google.com/webstore/detail/advanced-rest-client/hgmloofddffdnphfgcellkdfbfbjeloo?hl=en-US

It should run on localhost:8080


### Who do I talk to? ###

Abby Frank 
alfrank2014@gmail.com