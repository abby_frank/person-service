package com.person.Service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.person.model.Person;
import com.person.repository.PersonRepository;

@Service
public class PersonServiceImpl implements PersonService {

	@Autowired
	private PersonRepository personRepository;

	@Override
	public Person createPerson(Person person) {
		return personRepository.createPerson(person);
	}

	@Override
	public Person getPerson(Integer id) {
		return personRepository.findPerson(id);
	}
	
	@Override
	public Person updatePerson(Person person, String name, Integer familyId){
		return personRepository.updatePerson(person, name, familyId);
		
	}
	
	@Override
	public void deletePerson(Person person){
		personRepository.deletePerson(person);
	}
	
	@Override
	public List<Person> getFamily(Integer familyId){
		return personRepository.getFamily(familyId);
	}
	
	@Override
	public List<Person> createFamily(List<Person> familyMembers, Integer familyId){
		for(Person member : familyMembers){
			member.setFamilyId(familyId);
			personRepository.createPerson(member);
		}
		return familyMembers;
	}
	
	@Override
	public List<Person> linkFamily(List<Person> familyMembers, Integer familyId){
		for(Person member : familyMembers){
			member.setFamilyId(familyId);
			personRepository.updatePerson(member, member.getName(), familyId);
		}
		return familyMembers;
	}
		
}
