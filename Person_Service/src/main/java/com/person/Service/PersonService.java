package com.person.Service;

import java.util.List;

import com.person.model.Person;

public interface PersonService {

	Person createPerson(Person person);

	Person getPerson(Integer id);

	Person updatePerson(Person person, String name, Integer familyId);
	
	void deletePerson(Person person);
	
	List<Person> getFamily(Integer familyId);
	
	List<Person> createFamily(List<Person> familyMembers, Integer familyId);
	
	List<Person> linkFamily(List<Person> familyMembers, Integer familyId);
}