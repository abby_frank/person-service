package com.person.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.people.request.FamilyRequestWrapper;
import com.people.request.UpdateRequestWrapper;
import com.person.Service.PersonService;
import com.person.model.Person;

@RestController("controller")
public class PersonController {

		@Autowired
		private PersonService personService;
		
		public PersonController(){
			
		}
		
		@RequestMapping(value="/person", method = RequestMethod.POST, consumes="application/json",headers = "content-type=application/x-www-form-urlencoded")
		public ResponseEntity<Person> create(@RequestBody Person person){	
			Person value = personService.createPerson(person);
			return new ResponseEntity<Person>(value, HttpStatus.CREATED);
		}
		
		@RequestMapping(value = "/person/{id}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
		public ResponseEntity<Person> getPerson(@PathVariable Integer id){
			Person value = personService.getPerson(id);
			return new ResponseEntity<Person>(value, HttpStatus.OK);
		}
		
		@RequestMapping(value = "/person", method = RequestMethod.PUT, consumes="application/json", headers = "content-type=application/x-www-form-urlencoded")
		public ResponseEntity<Person> updatePerson(@RequestBody UpdateRequestWrapper request){
			Person value = personService.updatePerson(request.getPerson(), request.getName(), request.getFamilyId());
			return new ResponseEntity<Person>(value, HttpStatus.OK);		
		}
		
		@RequestMapping(value = "/person", method = RequestMethod.DELETE ,consumes="application/json",headers = "content-type=application/x-www-form-urlencoded")
		public ResponseEntity<Void> deletePerson(@RequestBody Person person){	
			personService.deletePerson(person);
			return new ResponseEntity<Void>(HttpStatus.OK);
		}
		
		@RequestMapping(value="/family/{id}", method=RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
		public ResponseEntity<List<Person>> getFamily(@PathVariable Integer id){
			List<Person> familyMembers = personService.getFamily(id);
			return new ResponseEntity<List<Person>>(familyMembers, HttpStatus.OK);
		}
		
		@RequestMapping(value = "/family", method = RequestMethod.POST ,consumes="application/json",headers = "content-type=application/x-www-form-urlencoded")
		public ResponseEntity<List<Person>> createNewFamily(@RequestBody FamilyRequestWrapper request){
			List<Person> newFamilyMembers = personService.createFamily(request.getFamilyMembers(), request.getFamilyId());
			return new ResponseEntity<List<Person>>(newFamilyMembers,HttpStatus.OK);
		}
		@RequestMapping(value = "/family", method = RequestMethod.PUT ,consumes="application/json",headers = "content-type=application/x-www-form-urlencoded")
		public ResponseEntity<List<Person>> linkFamily(@RequestBody FamilyRequestWrapper request){
			List<Person> familyMembers = personService.linkFamily(request.getFamilyMembers(), request.getFamilyId());
			return new ResponseEntity<List<Person>>(familyMembers, HttpStatus.OK);
		}
}