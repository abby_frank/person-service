package com.person.repository;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import javax.transaction.Transactional;
import org.springframework.stereotype.Repository;

import com.person.model.Person;

@Repository("personRepository")
public class PersonRepositoryImpl implements PersonRepository {
	
	private static final String getFamilyQuery = "Select person From Person person WHERE person.familyId = :familyId";
	@PersistenceContext
	private EntityManager entityManager;
	
	public PersonRepositoryImpl(){}

	@Override
	@Transactional
	public Person createPerson(Person person){
		entityManager.persist(person);
		return person;
	}
	
	@Override
	@Transactional
	public Person findPerson(Integer id){
		//TODO make if find person by name and not by the pk
		return entityManager.find(Person.class, id);		
	}
	
	@Override
	@Transactional
	public void deletePerson(Person person){
		entityManager.remove(entityManager.merge(person));		
	}

	@Override
	@Transactional
	public Person updatePerson(Person person, String name, Integer familyId) {
		Person personToBeUpdated = entityManager.find(Person.class, person.getId());
		personToBeUpdated.setFamilyId(familyId);
		personToBeUpdated.setName(name);
		return personToBeUpdated;	
	}
	
	@Override
	@Transactional
	public List<Person> getFamily(Integer familyId){
		TypedQuery<Person> query = entityManager.createQuery(getFamilyQuery, Person.class);
		query.setParameter("familyId", familyId);
		return query.getResultList();
		
	}
	
}
