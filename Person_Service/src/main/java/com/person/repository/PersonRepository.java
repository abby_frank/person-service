package com.person.repository;

import java.util.List;

import com.person.model.Person;

public interface PersonRepository {

	Person createPerson(Person person);

	Person findPerson(Integer id);
	
	Person updatePerson(Person person, String name, Integer familyId);
	
	void deletePerson(Person person);

	List<Person> getFamily(Integer familyId);
}