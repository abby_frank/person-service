package com.people.request;

import java.util.List;

import com.person.model.Person;

public class FamilyRequestWrapper {

	private List<Person> familyMembers;
	
	public List<Person> getFamilyMembers() {
		return familyMembers;
	}

	public void setFamilyMembers(List<Person> familyMembers) {
		this.familyMembers = familyMembers;
	}

	public Integer getFamilyId() {
		return familyId;
	}

	public void setFamilyId(Integer familyId) {
		this.familyId = familyId;
	}

	private Integer familyId;
}
